# AwesomeElixir

## Через докер

Настроить:

* Добавить пароль и логин юзера от гитхаба в `.env`
* `$ docker-compose build`
* `$ docker-compose run web mix ecto.create`
* `$ docker-compose run web mix ecto.migrate`

Запустить:

* `$ docker-compose up`

## Нативно

Настроить:

* Добавить пароль и логин юзера от гитхаба в `.env`
* `$ mix ecto.create`
* `$ mix ecto.migrate`

Запустить:

* `$ mix phx.server`

Приложение будет доступно по адресу http://localhost:4000

## Пометка
При запуске данные о библиотеках начнут собиратся. Библиотеки стягиваются по категориям по-этому на странице уже будут доступны стянутые библиотеки, пока остальные будут дособиратся.