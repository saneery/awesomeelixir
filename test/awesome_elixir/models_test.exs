defmodule AwesomeElixir.ModelsTest do
  use AwesomeElixir.DataCase

  alias AwesomeElixir.Models

  describe "libraries" do
    alias AwesomeElixir.Models.Library

    @valid_attrs %{
      last_commit: 42,
      name: "some name",
      stars: 42,
      description: "some description",
      url: "some url"
    }
    @update_attrs %{
      last_commit: 43,
      name: "some updated name",
      stars: 43,
      description: "some description",
      url: "some url"
    }
    @invalid_attrs %{last_commit: nil, name: nil, stars: nil}

    def library_fixture(attrs \\ %{}) do
      {:ok, category} = Models.create_category(%{name: "my category", description: "desc"})

      {:ok, library} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.put(:category_id, category.id)
        |> Models.create_library()

      library
    end

    test "list_libraries/0 returns all libraries" do
      library = library_fixture()
      assert Models.list_libraries() == [library]
    end

    test "get_library!/1 returns the library with given id" do
      library = library_fixture()
      assert Models.get_library!(library.id) == library
    end

    test "create_library/1 with valid data creates a library" do
      {:ok, category} = Models.create_category(%{name: "my category", description: "desc"})
      assert {:ok, %Library{} = library} = Models.create_library(Map.put(@valid_attrs, :category_id, category.id))
      assert library.last_commit == 42
      assert library.name == "some name"
      assert library.stars == 42
    end

    test "create_library/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Models.create_library(@invalid_attrs)
    end

    test "update_library/2 with valid data updates the library" do
      library = library_fixture()
      assert {:ok, %Library{} = library} = Models.update_library(library, @update_attrs)
      assert library.last_commit == 43
      assert library.name == "some updated name"
      assert library.stars == 43
    end

    test "update_library/2 with invalid data returns error changeset" do
      library = library_fixture()
      assert {:error, %Ecto.Changeset{}} = Models.update_library(library, @invalid_attrs)
      assert library == Models.get_library!(library.id)
    end

    test "delete_library/1 deletes the library" do
      library = library_fixture()
      assert {:ok, %Library{}} = Models.delete_library(library)
      assert_raise Ecto.NoResultsError, fn -> Models.get_library!(library.id) end
    end

    test "change_library/1 returns a library changeset" do
      library = library_fixture()
      assert %Ecto.Changeset{} = Models.change_library(library)
    end
  end

  describe "categories" do
    alias AwesomeElixir.Models.Category

    @valid_attrs %{description: "some description", name: "some name"}
    @update_attrs %{description: "some updated description", name: "some updated name"}
    @invalid_attrs %{description: nil, name: nil}

    def category_fixture(attrs \\ %{}) do
      {:ok, category} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Models.create_category()

      category
    end

    test "list_categories/0 returns all categories" do
      category = category_fixture()
      assert Models.list_categories() == []
    end

    test "get_category!/1 returns the category with given id" do
      category = category_fixture()
      assert Models.get_category!(category.id) == category
    end

    test "create_category/1 with valid data creates a category" do
      assert {:ok, %Category{} = category} = Models.create_category(@valid_attrs)
      assert category.description == "some description"
      assert category.name == "some name"
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Models.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      category = category_fixture()
      assert {:ok, %Category{} = category} = Models.update_category(category, @update_attrs)
      assert category.description == "some updated description"
      assert category.name == "some updated name"
    end

    test "update_category/2 with invalid data returns error changeset" do
      category = category_fixture()
      assert {:error, %Ecto.Changeset{}} = Models.update_category(category, @invalid_attrs)
      assert category == Models.get_category!(category.id)
    end

    test "delete_category/1 deletes the category" do
      category = category_fixture()
      assert {:ok, %Category{}} = Models.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> Models.get_category!(category.id) end
    end

    test "change_category/1 returns a category changeset" do
      category = category_fixture()
      assert %Ecto.Changeset{} = Models.change_category(category)
    end
  end
end
