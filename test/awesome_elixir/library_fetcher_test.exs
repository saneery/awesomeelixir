defmodule AwesomeElixir.Workers.LibraryFetcherTest do
  use AwesomeElixir.DataCase

  import Mox

  alias AwesomeElixir.{GitHubMock, Models}
  alias AwesomeElixir.Workers.LibraryFetcher

  setup :set_mox_global
  setup :verify_on_exit!

  test "fetch and import libs" do
    GitHubMock
    |> expect(:get!, fn _ ->
      content =
        File.read!("test/support/test_readme.md")
        |> Base.encode64()

      %{body: [content: content]}
    end)
    |> expect(:get, 4, fn
      "/repos/dalmatinerdb/dflow" ->
        {:ok, %{body: [stargazers_count: 8]}}
      "/repos/dalmatinerdb/dflow/commits" ->
        {:ok, %{body: commit()}}
      "/repos/takscape/elixir-array" ->
        {:ok, %{body: [stargazers_count: 8]}}
      "/repos/takscape/elixir-array/commits" ->
        {:ok, %{body: commit()}}
    end)

    LibraryFetcher.start_link([])

    Process.sleep(1_000)

    list = Models.list_categories()
    assert Enum.count(list) == 2

    list = Models.list_libraries()
    assert Enum.count(list) == 2
  end

  defp commit do
    %{commit: %{"author" => %{"date" => "2019-05-19T18:33:43Z"}}}
  end
end
