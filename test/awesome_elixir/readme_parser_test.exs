defmodule AwesomeElixir.ReadmeParserTest do
  use ExUnit.Case, async: true

  alias AwesomeElixir.ReadmeParser

  test "parse readme" do
    {content, _} =
      File.read!("test/support/test_readme.md")
      |> Earmark.parse()

    assert expected_list() == ReadmeParser.parse(content)
  end

  defp expected_list do
    [
      %{
        description: "*Libraries and implementations of algorithms and data structures.*",
        libraries: [
          %{
            description: "An Elixir wrapper library for Erlang's array.",
            name: "array",
            owner: "takscape",
            repo: "elixir-array",
            url: "https://github.com/takscape/elixir-array"
          }
        ],
        name: "Algorithms and Data structures"
      },
      %{
        description: "*Libraries and tools for working with actors and such.*",
        libraries: [
          %{
            description: "Pipelined flow processing engine.",
            name: "dflow",
            owner: "dalmatinerdb",
            repo: "dflow",
            url: "https://github.com/dalmatinerdb/dflow"
          }
        ],
        name: "Actors"
      }
    ]
  end
end
