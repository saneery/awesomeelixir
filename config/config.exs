# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :awesome_elixir,
  ecto_repos: [AwesomeElixir.Repo],
  http_adapter: AwesomeElixir.GitHub

config :awesome_elixir, AwesomeElixir.GitHub,
  user: System.get_env("GITHUB_USER"),
  password: System.get_env("GITHUB_PSWD")

fetcher_period =
  "FETCHER_PERIOD"
  |> System.get_env("24")
  |> Integer.parse()
  |> case do
    {num, ""} ->
      num

    _ ->
      24
  end

config :awesome_elixir, AwesomeElixir.Workers.LibraryFetcher,
  periodicity: fetcher_period,
  enabled: true

# Configures the endpoint
config :awesome_elixir, AwesomeElixirWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "aUB2UQBlmtbahAIqC0C3VkXuKBpWVdPrk+fh4IIouCMtBWSjToQ8zdHLmHyJ4iUv",
  render_errors: [view: AwesomeElixirWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AwesomeElixir.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
