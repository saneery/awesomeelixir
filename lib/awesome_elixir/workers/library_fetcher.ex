defmodule AwesomeElixir.Workers.LibraryFetcher do
  use GenServer
  require Logger
  alias AwesomeElixir.{ReadmeParser, Importer}

  @http_adapter Application.get_env(:awesome_elixir, :http_adapter)
  @periodicity Application.get_env(:awesome_elixir, __MODULE__)[:periodicity]

  def start_link(_) do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    send(self(), :update)
    {:ok, state}
  end

  def handle_info(:update, state) do
    update_storage()
    schedule_work()
    {:noreply, state}
  end

  defp schedule_work do
    Process.send_after(self(), :update, :timer.hours(@periodicity))
  end

  defp update_storage do
    {blocks, _} =
      @http_adapter.get!("/repos/h4cc/awesome-elixir/readme").body[:content]
      |> Base.decode64!(ignore: :whitespace)
      |> Earmark.parse()

    blocks
    |> ReadmeParser.parse()
    |> Enum.each(fn category ->
      category
      |> fetch_libs()
      |> Importer.import_category()
    end)
  end

  defp fetch_libs(%{libraries: libs} = category) do
    fetched_libs =
      libs
      |> Task.async_stream(&fetch_lib/1, [max_concurrency: 5, ordered: false])
      |> Stream.map(fn {_, v} -> v end)
      |> Stream.reject(&is_nil(&1))
      |> Enum.to_list()

    Logger.info("Category '#{category[:name]}' with libs is fetched")
    Map.put(category, :libraries, fetched_libs)
  end

  defp fetch_lib(%{repo: repo, owner: owner} = lib) do
    with {:ok, %{body: body}} <- @http_adapter.get("/repos/#{owner}/#{repo}"),
        [stargazers_count: stars] <- body,
        {:ok, %{body: commit}} <- @http_adapter.get("/repos/#{owner}/#{repo}/commits"),
        {:ok, last_update, _} <- DateTime.from_iso8601(get_in(commit, [:commit, "author", "date"])) do
      today =
        DateTime.utc_now()
        |> DateTime.to_unix()

      passed_days = round((today - DateTime.to_unix(last_update)) / 86400)

      %{
        stars: stars,
        last_commit: passed_days,
        name: lib[:name],
        description: lib[:description],
        url: lib[:url],
      }
    else
      _ ->
        nil
    end
  end
end
