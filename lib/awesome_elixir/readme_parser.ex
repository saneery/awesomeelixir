defmodule AwesomeElixir.ReadmeParser do
  alias Earmark.Block.{Heading, Para, List, ListItem}

  @doc "Parses .md file and returns list of maps"
  @spec parse(collection :: list()) :: list(map())
  def parse([_1lvl_header | list]) do
    parse(list, [])
  end

  # Matches with category and lists of libraries
  defp parse([%Heading{level: 2, content: name}, %Para{lines: lines}, %List{blocks: items} | collection], acc) do
    description = Enum.join(lines, "\n")

    libraries =
      items
      |> Enum.map(&Task.async(fn -> parse_library(&1) end))
      |> Enum.map(&Task.await/1)
      |> Enum.reject(&is_nil(&1))

    category = %{
      name: name,
      description: description,
      libraries: libraries
    }

    new_acc = [category | acc]

    parse(collection, new_acc)
  end

  defp parse([], acc), do: acc

  defp parse([%Heading{level: 1} | _], acc), do: acc

  defp parse([_ | collection], acc), do: parse(collection, acc)

  defp parse_library(%ListItem{blocks: [%Para{lines: [line]}]}) do
    with [_, name, url, description] <- Regex.run(~r/\[(\S+)\]\((\S+)\) - (.*)/, line),
         [_, owner, repo] <- Regex.run(~r/https:\/\/github.com\/([^\/]+)\/([^\/]+)/, url) do

      %{
        name: name,
        description: description,
        url: url,
        owner: owner,
        repo: repo
      }
    else
      _ ->
        nil
    end
  end
end
