defmodule AwesomeElixir.Importer do
  alias AwesomeElixir.Repo
  alias AwesomeElixir.Models
  alias AwesomeElixir.Models.Library

  def import_category(%{libraries: libs} = category) do
    {:ok, %{id: category_id}} = Models.create_category(category)
    libs = Enum.map(libs, &Map.put(&1, :category_id, category_id))

    Repo.insert_all(
      Library,
      libs,
      on_conflict: {:replace, [:stars, :last_commit, :description]},
      conflict_target: [:name, :url]
    )
  end
end
