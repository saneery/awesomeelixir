defmodule AwesomeElixir.GitHub do
  use HTTPoison.Base

  @expected_fields ~w(content stargazers_count commit)
  @github_api "https://api.github.com"

  def process_url(url) do
    @github_api <> url
  end

  def process_request_headers(headers) do
    user = get_config(:user)
    password = get_config(:password)
    credentials = Base.encode64("#{user}:#{password}")

    [{"Authorization", "Basic #{credentials}"} | headers]
  end

  def process_response_body(body) do
    body
    |> Jason.decode!()
    |> handle_result()
  end

  defp handle_result(%{"message" => "Moved Permanently", "url" => url}) do
    case get(String.replace(url, @github_api, "")) do
      {:ok, %HTTPoison.Response{body: body}} ->
        body

      _ ->
        []
    end
  end

  defp handle_result(map) when is_map(map) do
    map
    |> Map.take(@expected_fields)
    |> Enum.map(fn {k, v} -> {String.to_atom(k), v} end)
  end

  defp handle_result([head | _]) do
    head
    |> Map.take(@expected_fields)
    |> Enum.map(fn {k, v} -> {String.to_atom(k), v} end)
  end

  defp get_config(key) do
    :awesome_elixir
    |> Application.get_env(__MODULE__)
    |> Keyword.get(key)
  end
end
