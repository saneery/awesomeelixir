defmodule AwesomeElixir.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    base_children = [
      # Start the Ecto repository
      AwesomeElixir.Repo,
      # Start the endpoint when the application starts
      AwesomeElixirWeb.Endpoint
    ]

    children = base_children ++ childs()

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: AwesomeElixir.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def childs do
    [
      config(AwesomeElixir.Workers.LibraryFetcher)
    ]
    |> Enum.reject(&is_nil/1)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    AwesomeElixirWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp config(module) do
    if Application.get_env(:awesome_elixir, module)[:enabled] do
      module
    end
  end
end
