defmodule AwesomeElixir.Models.Library do
  use Ecto.Schema
  import Ecto.Changeset
  alias AwesomeElixir.Models.Category

  schema "libraries" do
    field :last_commit, :integer
    field :name, :string
    field :stars, :integer
    field :description, :string
    field :url, :string
    belongs_to(:category, Category)

    timestamps(inserted_at: false)
  end

  @doc false
  def changeset(library, attrs) do
    library
    |> cast(attrs, [:name, :stars, :last_commit, :description, :url, :category_id])
    |> validate_required([:name, :stars, :last_commit, :description, :url, :category_id])
  end
end
