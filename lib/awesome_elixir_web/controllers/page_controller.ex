defmodule AwesomeElixirWeb.PageController do
  use AwesomeElixirWeb, :controller
  alias AwesomeElixir.Models

  def index(conn, params) do
    min_stars = Map.get(params, "min_stars", 0)
    categories = Models.list_categories(min_stars)

    render(conn, "index.html", categories: categories)
  end
end
