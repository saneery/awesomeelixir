FROM elixir:1.9

RUN mkdir /app
RUN apt-get update && apt-get install -y inotify-tools
RUN curl -sL https://deb.nodesource.com/setup_11.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get install nodejs
COPY . /app
WORKDIR /app

RUN mix local.hex --force

RUN mix do compile