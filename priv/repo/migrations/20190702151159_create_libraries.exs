defmodule AwesomeElixir.Repo.Migrations.CreateLibraries do
  use Ecto.Migration

  def change do
    create table(:libraries) do
      add :name, :string
      add :stars, :integer
      add :last_commit, :integer
      add :category_id, references(:categories, on_delete: :nothing)
      add :description, :string
      add :url, :string
      add :updated_at, :naive_datetime, default: fragment("now()")
    end

    create index(:libraries, [:category_id])
    create unique_index(:libraries, [:name, :url])
  end
end
